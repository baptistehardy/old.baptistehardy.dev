package main

import (
	"html/template"
	"net/http"
	"os"
)

func main() {
	http.HandleFunc("/", router)

	http.Handle("/assets/", http.StripPrefix("/assets/", http.FileServer(http.Dir("assets"))))
	if err := http.ListenAndServe(":" + os.Getenv("PORT"), nil); err != nil {
		panic(err)
	}
}

var templates = template.Must(template.ParseGlob("templates/*"))

func router(w http.ResponseWriter, r *http.Request) {

	switch r.URL.Path {
		case "/":
			home(w, r)
			break
		default:
			errorHandler(w, r, http.StatusNotFound)
			return
	}
}

func home(w http.ResponseWriter, r *http.Request) {
	if err := templates.ExecuteTemplate(w, "main.html", nil); err != nil {
		panic(err)
	}
}

type Error struct {
	Status int
	Message string
}

func errorHandler(w http.ResponseWriter, r *http.Request, status int) {
	w.WriteHeader(status)
	httpError := Error{Status: status}

	switch status {
		case http.StatusNotFound:
			httpError.Message = "Page not found"
			break
		default:
			httpError.Message = "Unhandled error"
	}

	if err := templates.ExecuteTemplate(w, "error.html", httpError); err != nil {
		panic(err)
	}
}
