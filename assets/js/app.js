/*window.onload = function() {
    lax.setup({});
    document.addEventListener('scroll', () => lax.update(window.scrollY), false);
    lax.update(window.scrollY);
};

window.addEventListener("resize", function() {
    lax.populateElements()
});*/

window.addEventListener('load', function() {
    document.querySelector('#arrow').addEventListener('click', function (e) {
        e.preventDefault();
        document.querySelector('#experience').scrollIntoView({
            behavior: 'smooth',
            block: 'start'
        });
    });
    particlesJS.load('particles', '/assets/js/particles.json');
});